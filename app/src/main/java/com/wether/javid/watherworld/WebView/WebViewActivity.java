package com.wether.javid.watherworld.WebView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.wether.javid.watherworld.MyWidget.MyTextView;
import com.wether.javid.watherworld.MyWidget.MyTextViewAnimation;
import com.wether.javid.watherworld.PublicMethods;
import com.wether.javid.watherworld.R;

import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class WebViewActivity extends AppCompatActivity {
    WebView myWeb;
    Context mContext = WebViewActivity.this;
    LinearLayout animationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        onBind();
        getURL(getString(R.string.myPanelBazar));
    }

    private void onBind() {
        myWeb = findViewById(R.id.myWeb);
        animationView = findViewById(R.id.loading);
        animationView.setVisibility(View.GONE);
    }

    void getURL(String link) {
        animationView.setVisibility(View.VISIBLE);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(link, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toastMSg(mContext, getString(R.string.checkIntennetAccess));
            }

            @SuppressLint("SetJavaScriptEnabled")
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                myWeb.getSettings().setJavaScriptEnabled(true);
                myWeb.getSettings().setLoadWithOverviewMode(true);
                myWeb.getSettings().setUseWideViewPort(true);
                myWeb.getSettings().setBuiltInZoomControls(true);
                myWeb.getSettings().setPluginState(WebSettings.PluginState.ON);
                myWeb.setWebViewClient(new WebViewClient());
                myWeb.loadUrl("https://cafebazaar.ir/developer/nice_javid/?l=fa");
                Log.e("responseString  : ", responseString + "");
            }

            @Override
            public void onFinish() {
                super.onFinish();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animationView.setVisibility(View.GONE);
                    }
                }, 1500);
            }
        });
    }
}
