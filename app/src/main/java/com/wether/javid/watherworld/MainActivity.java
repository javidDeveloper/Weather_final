package com.wether.javid.watherworld;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.marcoscg.dialogsheet.DialogSheet;
import com.wether.javid.watherworld.FirstActivity.FirstActivity;

import com.wether.javid.watherworld.Models.WeatherDB;
import com.wether.javid.watherworld.MyWidget.MyImageView;
import com.wether.javid.watherworld.MyWidget.MyTextView;
import com.wether.javid.watherworld.MyWidget.MyTextViewAnimation;
import com.wether.javid.watherworld.RecyclerView.ForcastRecyclerAdapter;
import com.wether.javid.watherworld.WebView.WebViewActivity;
import com.wether.javid.watherworld.Yahoo.Condition;
import com.wether.javid.watherworld.Yahoo.Forecast;
import com.wether.javid.watherworld.Yahoo.YahooModel;

import java.util.List;
//
import cz.msebera.android.httpclient.Header;
import jp.wasabeef.recyclerview.adapters.SlideInRightAnimationAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    MyTextView submit, cityName, date, wind, windSpeed;
    RecyclerView recyclerView;
    MyImageView submitImg, cityItem;
    Context mContext = MainActivity.this;
    LinearLayout animationView;
    String subVal;
    Boolean RetryBack = false;
    FloatingActionButton fab;
    YahooModel yahoo;
    String txtCity;
    List<Forecast> forecast;
    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        final String value = intent.getStringExtra("cityName");
        getDataFromYahoo(value);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindView();
    }

    void bindView() {
        submit = findViewById(R.id.submit);
        wind = findViewById(R.id.wind);
        windSpeed = findViewById(R.id.windSpeed);
        cityName = findViewById(R.id.city_name);
        date = findViewById(R.id.date);
        recyclerView = findViewById(R.id.forecast_list);
        submitImg = findViewById(R.id.submitImg);
        cityItem = findViewById(R.id.cityItem);
        animationView = findViewById(R.id.loading);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
        animationView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(final View view) {
        int id = view.getId();
        if (id == R.id.fab) {
            new DialogSheet(this)
                    .setTitle(R.string.app_name)
                    .setMessage(getString(R.string.SaveString))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.YesString), new DialogSheet.OnPositiveClickListener() {
                        @Override
                        public void onClick(View v) {
                 for (Forecast fc : forecast) {
                            WeatherDB fcdb = new WeatherDB();
                            fcdb.setCity(txtCity);
                            fcdb.setText(fc.getText());
                            fcdb.setDay(fc.getDay());
                            fcdb.setDate(fc.getDate());
                            fcdb.setLow(fc.getLow());
                            fcdb.setHigh(fc.getHigh());
                            fcdb.setNow(PublicMethods.currentTime());
                            fcdb.save();
                        }
                        PublicMethods.snack(view,getString(R.string.saved));
                        }
                    })
                    .setNegativeButton(getString(R.string.NoString), new DialogSheet.OnNegativeClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    })

                    .setBackgroundColor(Color.WHITE) // Your custom background color
                    .setButtonsColorRes(R.color.colorPrimary)  // Default color is accent
                    .show();
            /////////////////////////////// add save forecast

        }
    }

    private void getDataFromYahoo(String cityValue) {
        animationView.setVisibility(View.VISIBLE);
        fab.setVisibility(View.GONE);
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                + cityValue + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toastMSg(mContext, getString(R.string.checkIntennetAccess));
                goTo(mContext, FirstActivity.class);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        animationView.setVisibility(View.GONE);
                        fab.setVisibility(View.VISIBLE);
                    }
                }, 1000);

            }
        });
    }

    private void parseData(String responseString) {
        Gson gson = new Gson();
        yahoo = gson.fromJson(responseString, YahooModel.class);
        try {
//            ShareClass shareClass = new ShareClass();
            String resultStr = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
            String result = (PublicMethods.ConvertFtoC(resultStr) + "°C");
            String windVal = yahoo.getQuery().getResults().getChannel().getWind().getDirection();
            String windSpeedVal = yahoo.getQuery().getResults().getChannel().getWind().getSpeed();
            wind.setText(windVal + "°");
            windSpeed.setText(windSpeedVal + " km");
//            shareClass.setDama(result);
            submit.setText(result);
            Intent intent = getIntent();
            final String value = intent.getStringExtra("cityName");
//            shareClass.setCity(value);
            cityName.setText(value);
            String toDay = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getDate();
            date.setText(toDay);
//            shareClass.setDate(toDay);
            subVal = PublicMethods.ConvertFtoC(resultStr);
            Condition resultPic = yahoo.getQuery().getResults().getChannel().getItem().getCondition();
            Log.e("_Text", resultPic.getText().toString());
//            shareClass.setText(resultPic.getText());
            if (resultPic.getText().toLowerCase().contains("partly" + " " + "cloudy")) {
                cityItem.loadURL(R.drawable.sunny_broken_cloudy);
            }
            if (resultPic.getText().toLowerCase().contains("breezy")) {
                cityItem.loadURL(R.drawable.sunny_broken_cloudy);
            }
            if (resultPic.getText().toLowerCase().contains("rain" + " " + "and" + " " + "snow")) {
                cityItem.loadURL(R.drawable.cloudy);
            }
            if (resultPic.getText().toLowerCase().contains("clear")) {
                cityItem.loadURL(R.drawable.sunny);
            }
            if (resultPic.getText().toLowerCase().contains("cloud")) {
                cityItem.loadURL(R.drawable.cloudy);
            }
            if (resultPic.getText().toLowerCase().contains("partly" + " " + "cloud")) {
                cityItem.loadURL(R.drawable.cloudy_sunny);
            }
            if (resultPic.getText().toLowerCase().contains("sun")) {
                cityItem.loadURL(R.drawable.sunny);

            }
            if (resultPic.getText().toLowerCase().contains("sun" + "cloud")) {
                cityItem.loadURL(R.drawable.cloudy_sunny);

            }
            if (resultPic.getText().toLowerCase().contains("rain")) {
                cityItem.loadURL(R.drawable.rain);

            }
            if (resultPic.getText().toLowerCase().contains("showers")) {
                cityItem.loadURL(R.drawable.rain_wo);

            }

            if (resultPic.getText().toLowerCase().toLowerCase().contains("snow")) {
                cityItem.loadURL(R.drawable.snow);

            }
            if (resultPic.getText().toLowerCase().contains("snow" + "w")) {
                cityItem.loadURL(R.drawable.snow_wo);
            }
            ///////////////////////////////forecast  list adapter
            ForcastRecyclerAdapter adapter = new ForcastRecyclerAdapter(mContext,
                    yahoo.getQuery().getResults().getChannel().getItem().getForecast());
            SlideInRightAnimationAdapter alphaAdapter = new SlideInRightAnimationAdapter(adapter);
            alphaAdapter.setDuration(1000);
            alphaAdapter.setFirstOnly(false);
            alphaAdapter.setInterpolator(new OvershootInterpolator());
            recyclerView.setAdapter(alphaAdapter);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayout.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            String submitVal = PublicMethods.ConvertFtoC(resultStr).trim();
            saveForcastToClipBoard(cityName.getText().toString(), yahoo.getQuery().getResults().getChannel().getItem().getForecast());
            Double HoC = Double.parseDouble(submitVal);
            if (HoC <= 25 && HoC >= 5) {
                submitImg.loadURL(R.drawable.normal);
            }
            if (HoC <= 26 && HoC >= 150) {
                submitImg.loadURL(R.drawable.high);
            }
            if (HoC <= 5) {
                submitImg.loadURL(R.drawable.low);
            }
        } catch (Exception e) {
            PublicMethods.toastMSg(mContext, getString(R.string.notFound));
            goTo(mContext, FirstActivity.class);
        }
    }

    public void saveForcastToClipBoard(String txtCityVal, List<Forecast> forecastval) {
      txtCity = txtCityVal;
      forecast = forecastval;
    }

    @Override
    public void onBackPressed() {
        if (RetryBack) {
            goTo(mContext, FirstActivity.class);
            finish();
        } else {
            RetryBack = true;
            Toast.makeText(mContext, R.string.dualBack, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RetryBack = false;
                }
            }, 2500);
        }
    }

    private void goTo(Context mContext, Class goTo) {
        Intent intent = new Intent(mContext, goTo);
        startActivity(intent);
        finish();
    }


}
