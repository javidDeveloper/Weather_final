package com.wether.javid.watherworld.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.wether.javid.watherworld.Models.WeatherDB;
import com.wether.javid.watherworld.R;
import com.wether.javid.watherworld.RecyclerView.DataBaseRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.SlideInRightAnimationAdapter;
public class SaveFargment extends Fragment {
    public static SaveFargment saveFargment;
    public static SaveFargment newInstance() {
        if (saveFargment == null)
            saveFargment = new SaveFargment();
        return saveFargment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.save_faragmant, container, false);
        final RecyclerView SaveRecycler;
        final List<WeatherDB> weatherList = new ArrayList<>();
        SaveRecycler = view.findViewById(R.id.SaveRecycler);
        getDataFromDatabase(weatherList);
        DataBaseRecyclerAdapter adapter = new DataBaseRecyclerAdapter(getActivity(), weatherList);
        adapter.notifyDataSetChanged();
        SlideInRightAnimationAdapter alphaAdapter = new SlideInRightAnimationAdapter(adapter);
        alphaAdapter.setDuration(1000);
        alphaAdapter.setFirstOnly(false);
        alphaAdapter.setInterpolator(new OvershootInterpolator());
        SaveRecycler.setAdapter(alphaAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        SaveRecycler.setLayoutManager(layoutManager);
        return view;
    }
    void getDataFromDatabase(List<WeatherDB> weatherList) {
        List<WeatherDB> weatherDBS = WeatherDB.listAll(WeatherDB.class);
        for (WeatherDB weatherDB : weatherDBS) {
            WeatherDB db = new WeatherDB();
            db.setDay(weatherDB.getDay());
            db.setDate(weatherDB.getDate());
            db.setCity(weatherDB.getCity());
            db.setHigh(weatherDB.getHigh());
            db.setLow(weatherDB.getLow());
            db.setNow(weatherDB.getNow());
            db.setText(weatherDB.getText());
            weatherList.add(db);
        }
    }
}

