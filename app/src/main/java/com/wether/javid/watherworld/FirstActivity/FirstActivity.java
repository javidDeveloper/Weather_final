package com.wether.javid.watherworld.FirstActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.marcoscg.dialogsheet.DialogSheet;
import com.wether.javid.watherworld.Fragments.MyPagerAdapter;
import com.wether.javid.watherworld.MyWidget.MyImageView;
import com.wether.javid.watherworld.MyWidget.MyTextViewAnimation;
import com.wether.javid.watherworld.PublicMethods;
import com.wether.javid.watherworld.R;
import com.wether.javid.watherworld.WebView.WebViewActivity;

public class FirstActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    MyImageView menuBtn;
    ViewPager firstViewPager;
    Context mContext = FirstActivity.this;
    Boolean RetryBack = false;
    View view;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        onBind();
        adapterViewPager();

    }

    private void onBind() {
        menuBtn = findViewById(R.id.menuBtn);
        navigationView = findViewById(R.id.navigationView);
        fab = findViewById(R.id.fab);
        drawerLayout = findViewById(R.id.navigation_drawer);
        menuBtn.setOnClickListener(this);
        fab.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.apps) {
                    PublicMethods.drawerCheck(drawerLayout);
                    Intent intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("title", R.string.about);
                    intent.putExtra("myLink", R.string.myPanelBazar);
                    startActivity(intent);
                }
                if (id == R.id.about) {
                    PublicMethods.drawerCheck(drawerLayout);
                    alertAbout();
                }

                return true;
            }
        });
    }

    private void adapterViewPager() {
        firstViewPager = findViewById(R.id.firstViewPager);
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), mContext);
        adapter.notifyDataSetChanged();
        firstViewPager.setAdapter(adapter);
        final NavigationTabStrip navigationTabStrip = findViewById(R.id.tabStrip);
        navigationTabStrip.setTitles(getString(R.string.SearchOnline), getString(R.string.SavedList));
        navigationTabStrip.setViewPager(firstViewPager);
    }

    @Override
    public void onBackPressed() {
        if (RetryBack) {
            finish();
        } else {
            PublicMethods.drawerCheck(drawerLayout);
            RetryBack = true;
            Toast.makeText(mContext, R.string.dualBack, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RetryBack = false;
                }
            }, 2500);
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.menuBtn) {
            drawerLayout.openDrawer(Gravity.RIGHT);
        }
        if (id == R.id.fab) {
            alertAbout();
        }
    }

    void alertAbout() {
//
        try {
            new DialogSheet(this)
                    .setTitle(R.string.about)
                    .setMessage(getString(R.string.developerMSGString)
                            + "\n" + getString(R.string.myName) + "\n" + getString(R.string.myEmail))
                    .setCancelable(true)
                    .setPositiveButton(getString(R.string.EmailString), new DialogSheet.OnPositiveClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto", getString(R.string.myEmail), null));
                            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.EmailSubject));
                            startActivity(Intent.createChooser(intent, getString(R.string.myEmail)));
                        }
                    })
                    .setNegativeButton(getString(R.string.NazarBazarString), new DialogSheet.OnNegativeClickListener() {
                        @Override
                        public void onClick(View v) {
//                            String url= "myket://comment?id=ir.mservices.mybook";
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getString(R.string.NazarBazar)));
                            startActivity(intent);
                        }
                    })

                    .setBackgroundColor(Color.WHITE) // Your custom background color
                    .setButtonsColorRes(R.color.colorPrimary)  // Default color is accent
                    .show();
        } catch (Exception e) {

            PublicMethods.snack(view, getString(R.string.EmailNotSendString));
        }

    }
}
