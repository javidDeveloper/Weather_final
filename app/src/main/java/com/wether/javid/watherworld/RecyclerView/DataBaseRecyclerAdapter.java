package com.wether.javid.watherworld.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.marcoscg.dialogsheet.DialogSheet;
import com.wether.javid.watherworld.Fragments.SaveFargment;
import com.wether.javid.watherworld.Models.WeatherDB;
import com.wether.javid.watherworld.MyWidget.MyTextView;
import com.wether.javid.watherworld.PublicMethods;
import com.wether.javid.watherworld.R;

import java.util.List;
import java.util.zip.Inflater;

public class DataBaseRecyclerAdapter extends RecyclerView.Adapter<DataBaseRecyclerAdapter.SaveViewHolder> {
    Context context;
//    View view;
    List<WeatherDB> weatherDBS;
    public DataBaseRecyclerAdapter(Context context, List<WeatherDB> weatherDBS) {
        this.context = context;
        this.weatherDBS = weatherDBS;
    }

    @NonNull
    @Override
    public DataBaseRecyclerAdapter.SaveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.save_forcast_list_item, parent, false);
        final SaveViewHolder holder = new SaveViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final DataBaseRecyclerAdapter.SaveViewHolder holder, final int position) {

        String low = weatherDBS.get(position).getLow();
        String high = weatherDBS.get(position).getHigh();
        holder.text.setText(weatherDBS.get(position).getText());
        holder.Day.setText(weatherDBS.get(position).getDay());
        holder.Day.setText(weatherDBS.get(position).getDay());
        holder.Date.setText(weatherDBS.get(position).getDate());
        holder.cityName.setText(weatherDBS.get(position).getCity());
        holder.timeSave.setText(weatherDBS.get(position).getNow());
        holder.Low.setText(PublicMethods.ConvertFtoC(low) + "°C");
        holder.High.setText(PublicMethods.ConvertFtoC(high) + "°C");
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                new DialogSheet(context)
                        .setTitle(R.string.app_name)
                        .setMessage(R.string.deleteString)
                        .setCancelable(true)
                        .setPositiveButton(R.string.YesString, new DialogSheet.OnPositiveClickListener() {
                            @Override
                            public void onClick(View v) {

                                weatherDBS.remove(position);
                                notifyItemRemoved(position);
                                notifyDataSetChanged();
                                WeatherDB db = WeatherDB.findById(WeatherDB.class, Long.valueOf(position+1));
                                Log.e("POSITON",db+"");
                                db.delete();
                                if(position<db.getId()){
                                    db.setId((long) (position+1));
                                }else {}
                            }
                        })
                        .setNegativeButton(R.string.NoString, new DialogSheet.OnNegativeClickListener() {
                            @Override
                            public void onClick(View v) {
//
                            }
                        })
                        .setBackgroundColor(Color.WHITE) // Your custom background color
                        .setButtonsColorRes(R.color.colorPrimary)  // Default color is accent
                        .show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return weatherDBS.size();
    }

    public class SaveViewHolder extends RecyclerView.ViewHolder {
        MyTextView Day, Date, Low, High, cityName, timeSave, text;
        CardView saveCard;
        ImageView trash;

        public SaveViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
            Day = itemView.findViewById(R.id.Day);
            Date = itemView.findViewById(R.id.Date);
            Low = itemView.findViewById(R.id.low);
            High = itemView.findViewById(R.id.high);
            cityName = itemView.findViewById(R.id.cityName);
            timeSave = itemView.findViewById(R.id.timeSave);
            saveCard = itemView.findViewById(R.id.saveCard);
            trash = itemView.findViewById(R.id.trash);
        }
    }

//    void alertAbout() {
//        try {
//            new DialogSheet(context)
//                    .setTitle(R.string.app_name)
//                    .setMessage(R.string.deleteString)
//                    .setCancelable(true)
//                    .setPositiveButton(R.string.YesString, new DialogSheet.OnPositiveClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                        }
//                    })
//                    .setNegativeButton(R.string.NoString, new DialogSheet.OnNegativeClickListener() {
//                        @Override
//                        public void onClick(View v) {
////
//                        }
//                    })
//                    .setBackgroundColor(Color.WHITE) // Your custom background color
//                    .setButtonsColorRes(R.color.colorPrimary)  // Default color is accent
//                    .show();
//        } catch (Exception e) {
//            PublicMethods.snack(view, R.string.EmailNotSendString + "");
//        }
//    }
}

