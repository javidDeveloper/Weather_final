package com.wether.javid.watherworld.MyWidget;


import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class MyImageView extends AppCompatImageView {
    private Context context;

    public MyImageView(Context context) {
        super(context);
        this.context = context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void loadURL(int url) {
        Picasso.with(context).load(url).into(this);
    }
}
