package com.wether.javid.watherworld.MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class MyTextView extends AppCompatTextView {
    private Context context;

    public MyTextView(Context context) {
        super(context);
        this.context = context;
        typeFace();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        typeFace();
    }

    public void typeFace() {
        Typeface Yekan = Typeface.createFromAsset(context.getAssets(), "font/Yekan.ttf");
        this.setTypeface(Yekan);
    }


}

