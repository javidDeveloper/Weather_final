package com.wether.javid.watherworld.FirstActivity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.wether.javid.watherworld.R;

public class LogoActivity extends AppCompatActivity {
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LogoActivity.this, FirstActivity.class);
                startActivity(intent);
                finish();
            }
        }, 5500);
    }

    @Override
    public void onBackPressed() {
    }
}
