package com.wether.javid.watherworld.MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

public class MyEditTextView extends AppCompatEditText {
    public MyEditTextView(Context context) {
        super(context);
        typeFace(context);
    }

    public MyEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        typeFace(context);
    }

    void typeFace(Context mContext) {
        Typeface yekan = Typeface.createFromAsset(mContext.getAssets(), "font/Yekan.ttf");
        this.setTypeface(yekan);
    }
}
