package com.wether.javid.watherworld.RecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wether.javid.watherworld.MyWidget.MyImageView;
import com.wether.javid.watherworld.MyWidget.MyTextView;
import com.wether.javid.watherworld.PublicMethods;
import com.wether.javid.watherworld.R;
import com.wether.javid.watherworld.Yahoo.Forecast;

import java.util.List;


public class ForcastRecyclerAdapter extends RecyclerView.Adapter<ForcastRecyclerAdapter.MyViewHolder> {
    Context mContext;
    List<Forecast> models;

    public ForcastRecyclerAdapter() {
    }

    public ForcastRecyclerAdapter(Context mContext, List<Forecast> models) {
        this.mContext = mContext;
        this.models = models;
        notifyDataSetChanged();
    }

    @Override
    public ForcastRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.forcast_card_item, parent, false);
        final MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ForcastRecyclerAdapter.MyViewHolder holder, final int position) {
        String pic = models.get(position).getText();
        String low = models.get(position).getLow();
        String high = models.get(position).getHigh();
        holder.day.setText(models.get(position).getDay());
        holder.date.setText(models.get(position).getDate());
        holder.low.setText(PublicMethods.ConvertFtoC(low));
        holder.high.setText(PublicMethods.ConvertFtoC(high));
        Log.e("_FText", models.get(position).getText());
        if (pic.toLowerCase().contains("partly" + " " + "cloudy")) {
            holder.img_result.loadURL(R.drawable.sunny_broken_cloudy);
        }
        if (pic.toLowerCase().contains("breezy")) {
            holder.img_result.loadURL(R.drawable.sunny_broken_cloudy);
        }
        if (pic.toLowerCase().contains("rain" + " " + "and" + " " + "snow")) {
            holder.img_result.loadURL(R.drawable.cloudy);
        }
        if (pic.toLowerCase().contains("clear")) {
            holder.img_result.loadURL(R.drawable.sunny);
        }
        if (pic.toLowerCase().contains("cloud")) {
            holder.img_result.loadURL(R.drawable.cloudy);
        }
        if (pic.toLowerCase().contains("partly" + " " + "cloud")) {
            holder.img_result.loadURL(R.drawable.cloudy_sunny);
        }
        if (pic.toLowerCase().contains("sun")) {
            holder.img_result.loadURL(R.drawable.sunny);
        }
        if (pic.toLowerCase().contains("sun" + "cloud")) {
            holder.img_result.loadURL(R.drawable.cloudy_sunny);
        }
        if (pic.toLowerCase().contains("rain")) {
            holder.img_result.loadURL(R.drawable.rain);
        }
        if (pic.toLowerCase().contains("showers")) {
            holder.img_result.loadURL(R.drawable.rain_wo);
        }
        if (pic.toLowerCase().toLowerCase().contains("snow")) {
            holder.img_result.loadURL(R.drawable.snow);
        }
        if (pic.toLowerCase().contains("snow" + "w")) {
            holder.img_result.loadURL(R.drawable.snow_wo);
        }
        holder.cardForecast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PublicMethods.toastMSg(mContext, position + 1 + " روز بعد ");
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        MyImageView img_result;
        MyTextView day, date, low, high;
        CardView cardForecast;

        public MyViewHolder(View itemView) {
            super(itemView);
            img_result = itemView.findViewById(R.id.img_result);
            day = itemView.findViewById(R.id.day);
            date = itemView.findViewById(R.id.date);
            low = itemView.findViewById(R.id.low);
            high = itemView.findViewById(R.id.high);
            cardForecast = itemView.findViewById(R.id.cardForecast);
        }
    }
}
