package com.wether.javid.watherworld.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wether.javid.watherworld.MainActivity;
import com.wether.javid.watherworld.MyWidget.MyButton;
import com.wether.javid.watherworld.MyWidget.MyEditTextView;
import com.wether.javid.watherworld.PublicMethods;
import com.wether.javid.watherworld.R;
public class SearchFragment extends Fragment {
    public static SearchFragment searchFragment;
    public static SearchFragment newInstance() {
        if (searchFragment == null) {
            searchFragment = new SearchFragment();
        }
        return searchFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        final MyEditTextView cityEditText;
        final MyButton searchBtn;
        cityEditText = view.findViewById(R.id.cityEditText);
        searchBtn = view.findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cityEditText.getText().toString().equals("")) {
                    PublicMethods.toastMSg(getActivity(), getString(R.string.pleaseEnterTheCityName));
                } else {
                    String value = cityEditText.getText().toString();
                    goToWithKeyVal(getActivity(), MainActivity.class, "cityName", value);
                }
            }
        });
        return view;
    }
    private void goToWithKeyVal(Context mContext, Class goTo, String key, String value) {
        Intent intent = new Intent(mContext, goTo);
        intent.putExtra(key, value);
        startActivity(intent);
        getActivity().finish();
    }
}
