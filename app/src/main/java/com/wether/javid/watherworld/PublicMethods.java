package com.wether.javid.watherworld;


import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wether.javid.watherworld.MyWidget.MyTextViewAnimation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PublicMethods {
    Context context;

    public static String ConvertFtoC(String fVar) {
        Double f = Double.parseDouble(fVar);
        double c = (f - 32) * 5 / 9;
        return (int) c + " ";
    }

    public static void toastMSg(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void drawerCheck(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
    }

    public static void snack(View view, String s) {
        Snackbar snackbar = Snackbar
                .make(view, s, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public static String currentTime() {
        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String date_str = df.format(cal.getTime());
        return date_str;
    }
}
