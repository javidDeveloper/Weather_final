package com.wether.javid.watherworld.MyWidget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.tomer.fadingtextview.FadingTextView;

public class MyTextViewAnimation extends FadingTextView {
    Context context;

    public MyTextViewAnimation(Context context) {
        super(context);
    }

    public MyTextViewAnimation(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyTextViewAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}

